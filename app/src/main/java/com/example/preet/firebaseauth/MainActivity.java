package com.example.preet.firebaseauth;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button buttonLogin;
    private EditText emailAddress;
    private EditText password;
    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressDialog;
    private TextView successLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        firebaseAuth = FirebaseAuth.getInstance();

        if (firebaseAuth.getCurrentUser() != null){
            //profile activity here
        }


        buttonLogin =  findViewById(R.id.loginButton);
        emailAddress= findViewById(R.id.editTextEmail);
        password = findViewById(R.id.editTextPassword);
        progressDialog = new ProgressDialog(this);
        successLogin = findViewById(R.id.textViewLogin);

        buttonLogin.setOnClickListener(this);
    }

    private void userLogin(){
        String email = emailAddress.getText().toString().trim();
        String pass = password.getText().toString().trim();

        if (TextUtils.isEmpty(email)){
            Toast.makeText(this, "Please enter email",Toast.LENGTH_LONG).show();
            return;
        }

        if(TextUtils.isEmpty(pass)){
            Toast.makeText(this,"Please enter password", Toast.LENGTH_LONG).show();
            return;
        }

        progressDialog.setMessage("Logging in... please wait.");
        progressDialog.show();

        firebaseAuth.signInWithEmailAndPassword(email,pass).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                    progressDialog.dismiss();

                if (task.isSuccessful()){
                    successLogin.setText("Login Successful");


                }
            }


        });


    }

    @Override
    public void onClick(View v) {
        if (v == buttonLogin){
            userLogin();
        }


    }
}


